import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';


@Component({
  selector: 'app-complete',
  templateUrl: './complete.page.html',
  styleUrls: ['./complete.page.scss'],
})
export class CompletePage implements OnInit {

  constructor(
    private router: Router,
    private translate: TranslateService
    ) { }

  ngOnInit() {
  }
  continue() {
    this.router.navigateByUrl('/emergency');
}
}
