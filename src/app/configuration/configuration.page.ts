import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-configuration',
  templateUrl: './configuration.page.html',
  styleUrls: ['./configuration.page.scss'],
})
export class ConfigurationPage implements OnInit {

  constructor(
    private router: Router,
    private translate: TranslateService
    ) { }

  ngOnInit() {
  }
  accept() {
    this.router.navigateByUrl('/complete');
}
}