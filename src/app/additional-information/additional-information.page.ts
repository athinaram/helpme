import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-additional-information',
  templateUrl: './additional-information.page.html',
  styleUrls: ['./additional-information.page.scss'],
})
export class AdditionalInformationPage implements OnInit {

  constructor(
    private router: Router,
    private translate: TranslateService
    ) { }

  ngOnInit() {
  }

  continue() {
    this.router.navigateByUrl('/configuration');
  }

}
